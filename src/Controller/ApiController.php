<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractController
{
    /**
     * @Route("/user/list", name="user_list", methods={"GET"})
     */
    public function list()
    {
        /** @var User $user */
        $userRepository = $this->getDoctrine()->getRepository(User::class);

        return new JsonResponse($userRepository->getAllUsers(), 200);
    }

    /**
     * @Route("/show/{id}", name="user_show", methods={"GET"})
     */
    public function show($id, Request $request)
    {
        /** @var User $user */
        $userRepository = $this->getDoctrine()->getRepository(User::class);

        $user = $userRepository->getUserById($id);

        return new JsonResponse($user, 200);
    }

    /**
     * @Route("/create", name="user_create", methods={"POST"})
     */
    public function create(Request $request)
    {
        $data = $request->request->all();

        $user = new User();
        $user->setFirstname($data['firstname']);
        $user->setLastname($data['lastname']);
        $user->setPosition($data['position']);
        $user->setCreatedAt(new \DateTime());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/update/{id}", name="user_update", methods={"POST"})
     */
    public function update(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();

        /** @var User $user */
        $user = $em->getRepository(User::class)->find($id);
        $user->setFirstname($data['firstname']);
        $user->setLastname($data['lastname']);
        $user->setPosition($data['position']);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse(['success' => true], 200);
    }

    /**
     * @Route("/delete/{id}", name="user_delete", methods={"GET"})
     */
    public function delete($id)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            /** @var User $user */
            $user = $em->getRepository(User::class)->find($id);

            $em->remove($user);
            $em->flush();
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false], 200);
        }

        return new JsonResponse(['success' => true], 200);
    }
}
